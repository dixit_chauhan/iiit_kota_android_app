package in.forsk.iiitk;


import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Feedback extends Activity {
	  
	  String br;
	  ProgressDialog pDialog;
	  
	  private String URL_NEW_CATEGORY = "http://192.168.229.1/iiitk/android/feedback.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        
        final EditText name;
		final EditText email;
		final EditText mobile;
		final EditText comment;
        
    	
    	name = (EditText) findViewById(R.id.etName);
    	email = (EditText) findViewById(R.id.etMobile);
    	mobile = (EditText) findViewById(R.id.etEmail);
    	comment = (EditText) findViewById(R.id.etComment);
    	
    	   final Button b= (Button)findViewById(R.id.button_submit);
    	   
    	   
    	   
    	   b.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
				
							String s1 = name.getText().toString();    //Regno
							String s2 = email.getText().toString();   //username
							String s3 = mobile.getText().toString();   //password
							String s4 = comment.getText().toString();  
								
							
							String a[] = {s1,s2,s3,s4};
							
							if( s1.length()>0 && s2.length()>0 && s3.length()>0 && s4.length()>0 )
							{
								
								
							new AddNewCategory().execute(a);	
							
						
							
							}
							else{
								Toast.makeText(Feedback.this, "Please Fill all details", 2000).show();
							}
							
						}				
			});
    	   
    	   
    	   

    }

    
    
    /**
	 * Async task to create a new food category
	 * */
	private class AddNewCategory extends AsyncTask<String, Void, Void> {

		boolean isNewCategoryCreated = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Feedback.this);
			pDialog.setMessage("Registering new entries..");
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(String... arg) {

			String s1 = arg[0];
			String s2 = arg[1];
			String s3 = arg[2];
			String s4 = arg[3];
						

			// Preparing post params
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("name", s1));
			params.add(new BasicNameValuePair("mobile", s2));
			params.add(new BasicNameValuePair("email", s3));			
			params.add(new BasicNameValuePair("comment", s4));
	

			ServiceHandler serviceClient = new ServiceHandler();

			String json = serviceClient.makeServiceCall(URL_NEW_CATEGORY,
					ServiceHandler.POST, params);

			Log.d("Create Response: ", "> " + json);

			if (json != null) {
				try {
					JSONObject jsonObj = new JSONObject(json);
					boolean error = jsonObj.getBoolean("error");
					// checking for error node in json
					if (!error) {	
						// new category created successfully
						isNewCategoryCreated = true;
					} else {
						Log.e("Create Category Error: ", "> " + jsonObj.getString("message"));
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			} else {
				Log.e("JSON Data", "Didn't receive any data from server!");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();
			if (isNewCategoryCreated) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(Feedback.this, "feedback submitted", 3000).show();
						Intent i = new Intent(getApplicationContext(),Feedback.class);
						startActivity(i);
						finish();
					}
				});
			}
		}
	}

    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.feedback, menu);
        return true;
    }
    
}
